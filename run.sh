ln -s /temp/app/* /app/

bundle install --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` --retry 3

rails db:create
rails db:migrate

rm -rf /app/tmp/pids/server.pid

rails s -p 3000 -b '0.0.0.0'