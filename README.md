# Rails Build Kit

This project has a minimal setup needed to run Rails in a Docker environment.

## Configuration

### 1. Docker Compose (with SQLlite)

Add a `docker-compose.yml` to your project with the following configuration:

```
version: '2'
services:
  web:
    container_name: rails
    image: registry.gitlab.com/wahomekevin/build-rails-kit
    command: sh /temp/app/run.sh
    volumes:
      - .:/app
    ports:
      - "3000:3000"
```

### 2. Docker Compose (with Postgres)

Add a `docker-compose.yml` to your project with the following configuration:

```
version: '2'
services:
  db:
    image: postgres:10.1-alpine
  web:
    container_name: rails
    image: registry.gitlab.com/wahomekevin/build-rails-kit
    command: sh /temp/app/run.sh
    volumes:
      - .:/app
    ports:
      - "3000:3000"
    depends_on:
      - db
```

### 2. Database

This container uses a Postgres image as the database. As a result, you will need to configure your username and password to make it work.
The default config can be added in the following way:

Update the `config/database.yml` default database config to the following:
```
host: db
username: postgres
```

## Run The Project

Run the following command in your terminal:
`$ docker-compose up`

This does the following:
```
- Creates the App container, and Database container
- Installs all gems needed for the project
- Runs all migrations
```

## Access The Container

To get shell access to your created container, run the following command:

`$ docker exec -ti rails sh`

## Troubleshooting

1. Rails not Found

When a Rails version is specified in the `Gemfile`, the following error occurs:
`Your Ruby version is 2.4.2, but your Gemfile specified 2.6.0`

To bypass this error, remove the Rails version requirement from your Gemfile. This is a temporary fix, but makes the project run fine.