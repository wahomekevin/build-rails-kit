FROM ruby:2.6.0-alpine3.7
LABEL maintainer="wahomekevin@gmail.com"

# Minimal requirements to run a Rails app
RUN apk add --no-cache --update build-base \
                                linux-headers \
                                git \
                                postgresql-dev \
                                sqlite-dev \
                                ruby-dev \
                                nodejs \
                                tzdata


COPY [ "run.sh", "/temp/app/" ]

ENV APP_PATH /app

WORKDIR $APP_PATH

EXPOSE 3000
